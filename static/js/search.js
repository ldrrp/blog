/* 
 * FivePM Search
 *
 * A site search library built to use the GitLab search API for searching posts on static sites built with tools like Hugo
 * 
 */
(function(window) {

    /* Define the search module */
    const _5pms = {

        /* Define the settings for the search module */
        Settings: {
            API: null, /* Which API are we using (GitLab, GitHub) */
            Project: null, /* Project ID that we are searching */
            Token: null, /* Auth Token - Personal Access Token in GitLab */
            Directory: null, /* Directory that we want to search in the system */
            ShowResults: 10,
            Container: null, /* ID of container for the search */
            GitLab: { /* GitLab Settings - things we need to define to work with GitLab - These are defaults, we shouldnt need to touch anything here unless the API is updated */
                SearchURL: 'https://gitlab.com/api/v4/projects/{{PROJECT}}/search?scope=blobs&search=',
                FileURL: 'https://gitlab.com/api/v4/projects/{{PROJECT}}/repository/files/',
                Headers: {
                    'PRIVATE-TOKEN': null
                }
            }
        },

        /* Hold the results of the search */
        Results: {
            Count: 0,
            Pages: 1,
            Page: 1,
            Files: [],
            Data: {},

            /* Load the next page */
            Next: async () => {
                /* If a container is defined we want to build the results into it */
                if (_5pms.Settings.Container) {
                    _5pms.Results._build(await _5pms._getPageData(_5pms.Results.Page + 1));
                } else {
                    return await _5pms._getPageData(_5pms.Results.Page + 1);
                }
            },

            /* Get the data for a specific page */
            GetPage: async (page = 1) => {
                return await _5pms._getPageData(page);
            },

            /* Load the previous page */
            Previous: async () => {
                /* If a container is defined we want to build the results into it */
                if (_5pms.Settings.Container) {
                    _5pms.Results._build(await _5pms._getPageData(_5pms.Results.Page - 1));
                } else {
                    return await _5pms._getPageData(_5pms.Results.Page - 1);
                }
            },

            /* Reset results object */
            Reset: () => {
                _5pms.Results.Count = 0;
                _5pms.Results.Pages = 1;
                _5pms.Results.Page = 1;
                _5pms.Results.Files = [];
                _5pms.Results.Data = {};
            },
            
            /* Build the default page for search results */
            _build: (results = null) => {
                /* Return early */
                if (!results) {
                    return;
                }
                
                /* Get the result container and clear it */
                let container = document.querySelector('#five-pm-search-results #list');
                container.innerHTML = "";
                
                /* Get site base url */
                let baseURL = `${window.location.protocol}//${window.location.host}`;
                
                /* Build the result list */
                results.forEach(data => {
                    /* Get full url for the post */
                    let url = baseURL + data.url;
                    
                    /* Build the tags for the post */
                    let tags = '';
                    data.tags.forEach(tag => {
                        tags = `${tags}<div class="tag">${tag}</div>`
                    });
                    
                    /* Create a new result */
                    let result = document.createElement('div');
                    result.classList.add('result');
                    result.innerHTML = `
                        <div class="title"><a href="${url}" title="${data.title}">${data.title}</a></div>
                        <div class="link">${url}</div>
                    `;

                    if (data.subtitle) {
                        result.innerHTML += `<div class="sub-title">${data.subtitle}</div>`;
                    }

                    if (data.image) {
                        result.innerHTML += `
                            <div class="image">
                                <a href="${url}" title="${data.title}"><img src="${data.image}" alt="${data.title}"/></a>
                            </div>
                        `;
                    }

                    if (data.excerpt) {
                        result.innerHTML += `<div class="excerpt">${data.excerpt}</div>`;
                    }

                    if (tags) {
                        result.innerHTML += `<div class="tags">${tags}</div>`;
                    }

                    if (data.author) {
                        result.innerHTML += `<div class="author">${data.author}</div>`;
                    }

                    if (data.date) {
                        result.innerHTML += `<div class="date">${data.date}</div>`;
                    }

                    /* Add the link to the page */
                    container.appendChild(result)
                });
            },

            /* Handle loader operations */
            _loader: {

                /* Add the loader to the page */
                Start: () => {
                    document.querySelector('#five-pm-search #five-pm-search-results #loader').classList.add('on');
                },

                /* Remove the loader from the page */
                Stop: () => {
                    document.querySelector('#five-pm-search #five-pm-search-results #loader').classList.remove('on');
                }
            }
        },

        /* Initialize the Search module */
        /* @TODO: make sure all required settings are set */
        Init: (settings) => {
            /* Set the required settings */
            _5pms.Settings.API = settings.API;
            _5pms.Settings.Token = settings.Token;
            _5pms.Settings.Project = settings.Project;
            _5pms.Settings.Directory = settings.Directory;
            
            /* If a container is passed in set it up */
            if (settings.Container) {
                _5pms.Settings.Container = document.querySelector(`#${settings.Container}`);
                _5pms._container();
            }
        },
        
        /* Setup the search container with the searchbox, button, and results fields */
        _container: () => {
            
            /* Create the page */
            let page = document.createElement('div');
            page.id = 'five-pm-search';
            page.innerHTML = `
                <div id="five-pm-search-box">
                    <input id="five-pm-search-query" type="text" placeholder="Search"/>
                </div>
                <div id="five-pm-search-tools">
                    <div id="five-pm-search-stats"></div>
                    <div id="five-pm-search-sort"></div>
                </div>
                <div id="five-pm-search-results">
                    <div id="list"></div>
                    <div id="pagination"></div>
                    <div id="five-pm-search-credit"><a href="https://search.5pm.dev">Powered by 5pm.dev Search</a></div>
                    <div id="loader">
                        <div class="cssload-thecube">
                            <div class="cssload-cube cssload-c1"></div>
                            <div class="cssload-cube cssload-c2"></div>
                            <div class="cssload-cube cssload-c4"></div>
                            <div class="cssload-cube cssload-c3"></div>
                        </div>
                    </div>
                </div>
            `;
            
            /* Add the default style sheet to he page */
            let stylesheet = document.createElement('style');
            stylesheet.type = 'text/css';
            stylesheet.innerHTML = `
                #five-pm-search {
                    display: grid;
                    grid-row-gap: 1em;
                    grid-template-rows: 8em 4em auto;
                    width: 100%;
                    height: 100%;
                    font-size: 10px;
                    border-radius: 0.2em;
                    background: #ffffff;
                }
                #five-pm-search #five-pm-search-box {
                    display: grid;
                    justify-items: center;
                    align-items: center;
                }
                #five-pm-search #five-pm-search-box #five-pm-search-query {
                    height: 3em;
                    width: 100%;
                    max-width: 30em;
                    padding: 0 1em;
                    text-align: center;
                    border: 1px solid lightgrey;
                    border-radius: 3em;
                    outline: none;
                }
                #five-pm-search #five-pm-search-box #five-pm-search-button {}
                #five-pm-search #five-pm-search-tools #five-pm-search-stats {}
                #five-pm-search #five-pm-search-tools #five-pm-search-sort {}
                
                #five-pm-search #five-pm-search-results {
                    display: grid;
                    position: relative;
                    grid-template-rows: auto 4em;
                    grid-template-columns: 1fr 1fr;
                    grid-template-areas: 
                        "list list"
                        "pagination credit";
                    height: 100%;
                    overflow-x: hidden;
                    overflow-y: scroll;
                }
                #five-pm-search #five-pm-search-results #list {
                    grid-area: list;
                }
                #five-pm-search #five-pm-search-results #list .result {
                    display: grid;
                    grid-template-rows: 3em 2em 2em auto 2em 2em;
                    grid-template-columns: 21em 1fr 1fr;
                    grid-row-gap: 0.75em;
                    grid-template-areas: 
                        "title title title" 
                        "link link link" 
                        "image subtitle subtitle"
                        "image excerpt excerpt"
                        "image tags tags"
                        "image author date";
                    padding: 0 2em 2em 2em;
                }
                #five-pm-search #five-pm-search-results #list .result a {
                    justify-content: left;
                }
                #five-pm-search #five-pm-search-results #list .result .title {
                    grid-area: title;
                    font-size: 2em;
                }
                #five-pm-search #five-pm-search-results #list .result .link {
                    grid-area: link;
                    font-size: 1.25em;
                }
                #five-pm-search #five-pm-search-results #list .result .sub-title {
                    grid-area: subtitle;
                    font-size: 1.5em;
                }
                #five-pm-search #five-pm-search-results #list .result .image {
                    grid-area: image;
                }
                #five-pm-search #five-pm-search-results #list .result .image img {
                    max-width: 20em;
                }
                #five-pm-search #five-pm-search-results #list .result .excerpt {
                    grid-area: excerpt;
                    font-size: 1.25em;
                }
                #five-pm-search #five-pm-search-results #list .result .tags {
                    grid-area: tags;
                    font-size: 1.25em;
                }
                #five-pm-search #five-pm-search-results #list .result .tags .tag {
                    display: inline-block;
                    padding: 0 0.5em;
                }
                #five-pm-search #five-pm-search-results #list .result .author {
                    grid-area: author;
                    font-size: 1.25em;
                }
                #five-pm-search #five-pm-search-results #list .result .date {
                    grid-area: date;
                    font-size: 1.25em;
                }
                #five-pm-search #five-pm-search-results #pagination {
                    grid-area: pagination;
                    justify-self: left;
                    padding: 0 0 0 2em;
                    line-height: 4em;
                }

                #five-pm-search #five-pm-search-results #loader {
                    display: none;
                }
                #five-pm-search #five-pm-search-results #loader.on {
                    display: block;
                    position: absolute;
                    left: calc(50% - 13px);
                    top: calc(50% - 18px);
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube {
                    width: 26px;
                    height: 26px;
                    margin: 0 auto;
                    margin-top: 17px;
                    position: relative;
                    transform: rotateZ(45deg);
                        -o-transform: rotateZ(45deg);
                        -ms-transform: rotateZ(45deg);
                        -webkit-transform: rotateZ(45deg);
                        -moz-transform: rotateZ(45deg);
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-cube {
                    position: relative;
                    transform: rotateZ(45deg);
                        -o-transform: rotateZ(45deg);
                        -ms-transform: rotateZ(45deg);
                        -webkit-transform: rotateZ(45deg);
                        -moz-transform: rotateZ(45deg);
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-cube {
                    float: left;
                    width: 50%;
                    height: 50%;
                    position: relative;
                    transform: scale(1.1);
                        -o-transform: scale(1.1);
                        -ms-transform: scale(1.1);
                        -webkit-transform: scale(1.1);
                        -moz-transform: scale(1.1);
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-cube:before {
                    content: "";
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background-color: rgb(203,232,150);
                    animation: cssload-fold-thecube 3.48s infinite linear both;
                        -o-animation: cssload-fold-thecube 3.48s infinite linear both;
                        -ms-animation: cssload-fold-thecube 3.48s infinite linear both;
                        -webkit-animation: cssload-fold-thecube 3.48s infinite linear both;
                        -moz-animation: cssload-fold-thecube 3.48s infinite linear both;
                    transform-origin: 100% 100%;
                        -o-transform-origin: 100% 100%;
                        -ms-transform-origin: 100% 100%;
                        -webkit-transform-origin: 100% 100%;
                        -moz-transform-origin: 100% 100%;
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-c2 {
                    transform: scale(1.1) rotateZ(90deg);
                        -o-transform: scale(1.1) rotateZ(90deg);
                        -ms-transform: scale(1.1) rotateZ(90deg);
                        -webkit-transform: scale(1.1) rotateZ(90deg);
                        -moz-transform: scale(1.1) rotateZ(90deg);
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-c3 {
                    transform: scale(1.1) rotateZ(180deg);
                        -o-transform: scale(1.1) rotateZ(180deg);
                        -ms-transform: scale(1.1) rotateZ(180deg);
                        -webkit-transform: scale(1.1) rotateZ(180deg);
                        -moz-transform: scale(1.1) rotateZ(180deg);
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-c4 {
                    transform: scale(1.1) rotateZ(270deg);
                        -o-transform: scale(1.1) rotateZ(270deg);
                        -ms-transform: scale(1.1) rotateZ(270deg);
                        -webkit-transform: scale(1.1) rotateZ(270deg);
                        -moz-transform: scale(1.1) rotateZ(270deg);
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-c2:before {
                    animation-delay: 0.44s;
                        -o-animation-delay: 0.44s;
                        -ms-animation-delay: 0.44s;
                        -webkit-animation-delay: 0.44s;
                        -moz-animation-delay: 0.44s;
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-c3:before {
                    animation-delay: 0.87s;
                        -o-animation-delay: 0.87s;
                        -ms-animation-delay: 0.87s;
                        -webkit-animation-delay: 0.87s;
                        -moz-animation-delay: 0.87s;
                }
                #five-pm-search #five-pm-search-results #loader.on .cssload-thecube .cssload-c4:before {
                    animation-delay: 1.31s;
                        -o-animation-delay: 1.31s;
                        -ms-animation-delay: 1.31s;
                        -webkit-animation-delay: 1.31s;
                        -moz-animation-delay: 1.31s;
                }
                
                @keyframes cssload-fold-thecube {
                    0%, 10% {
                        transform: perspective(49px) rotateX(-180deg);
                        opacity: 0;
                    }
                    25%, 75% {
                        transform: perspective(49px) rotateX(0deg);
                        opacity: 1;
                    }
                    90%, 100% {
                        transform: perspective(49px) rotateY(180deg);
                        opacity: 0;
                    }
                }
                
                @-o-keyframes cssload-fold-thecube {
                    0%, 10% {
                        -o-transform: perspective(49px) rotateX(-180deg);
                        opacity: 0;
                    }
                    25%, 75% {
                        -o-transform: perspective(49px) rotateX(0deg);
                        opacity: 1;
                    }
                    90%, 100% {
                        -o-transform: perspective(49px) rotateY(180deg);
                        opacity: 0;
                    }
                }
                
                @-ms-keyframes cssload-fold-thecube {
                    0%, 10% {
                        -ms-transform: perspective(49px) rotateX(-180deg);
                        opacity: 0;
                    }
                    25%, 75% {
                        -ms-transform: perspective(49px) rotateX(0deg);
                        opacity: 1;
                    }
                    90%, 100% {
                        -ms-transform: perspective(49px) rotateY(180deg);
                        opacity: 0;
                    }
                }
                
                @-webkit-keyframes cssload-fold-thecube {
                    0%, 10% {
                        -webkit-transform: perspective(49px) rotateX(-180deg);
                        opacity: 0;
                    }
                    25%, 75% {
                        -webkit-transform: perspective(49px) rotateX(0deg);
                        opacity: 1;
                    }
                    90%, 100% {
                        -webkit-transform: perspective(49px) rotateY(180deg);
                        opacity: 0;
                    }
                }
                
                @-moz-keyframes cssload-fold-thecube {
                    0%, 10% {
                        -moz-transform: perspective(49px) rotateX(-180deg);
                        opacity: 0;
                    }
                    25%, 75% {
                        -moz-transform: perspective(49px) rotateX(0deg);
                        opacity: 1;
                    }
                    90%, 100% {
                        -moz-transform: perspective(49px) rotateY(180deg);
                        opacity: 0;
                    }
                }
                
                #five-pm-search #five-pm-search-credit {
                    grid-area: credit;
                    justify-self: right;
                    padding: 0 2em 0 0;
                    line-height: 4em;
                }
                #five-pm-search #five-pm-search-credit a {}
            `;
            
            /* Add the page and stylesheet to the container passed in from settings */
            _5pms.Settings.Container.appendChild(stylesheet);
            _5pms.Settings.Container.appendChild(page);
            
            /* Add event listener to the box */
            document.querySelector('#five-pm-search-query').addEventListener('keyup', (event) => {
                /* Listen for enter button */
                if (event.keyCode === 13) {
                    FivePM.Search.Query(document.querySelector('#five-pm-search-query').value, _5pms.Results._build);
                }
            });
        },

        /* Send the search request to the API */
        /* @TODO: this may be pretty set to GitLab, need to make it support GitHub */
        Query: async (string, _callback) => {

            /* Reset the result set */
            _5pms.Results.Reset();

            /* Start the loader */
            _5pms.Results._loader.Start();

            /* Set the API URL */
            let url = _5pms.Settings[_5pms.Settings.API].SearchURL.replace('{{PROJECT}}', _5pms.Settings.Project) + encodeURI(string);

            /* Make the request */
            let response = await _5pms.Fetch(url);

            /* Get all the files from the result set */
            response.forEach(result => {
                /* Add the file to the array if it is not already there */
                if (result.basename.includes(_5pms.Settings.Directory) && !_5pms.Results.Files.includes(result.path)) {
                    _5pms.Results.Files.push(result.path);
                }
            });

            /* Fill out the result data */
            _5pms.Results.Count = _5pms.Results.Files.length;
            _5pms.Results.Pages = Math.ceil(_5pms.Results.Count / _5pms.Settings.ShowResults);

            /* Use the callback if provided */
            if (_callback) {
                _callback(await _5pms._getPageData(_5pms.Results.Page));
            } else {
                return await _5pms._getPageData(_5pms.Results.Page);
            } 
        },

        /* Get file content for the files in the page */
        _getPageData: async (page = 1) => {

            /* Get the ending and starting index */
            let pageEnd = _5pms.Settings.ShowResults * page;
            let pageStart = pageEnd - 10;

            /* Results to return */
            let results = [];
            
            for (r = pageStart; r < pageEnd; ++r) {

                /* If the index is out of range quit the loop early */
                if (!_5pms.Results.Files[r]) {
                    break;
                }

                /* If the data is already in the system just return it */
                if (_5pms.Results.Data[_5pms.Results.Files[r]]) {
                    results.push(_5pms.Results.Data[_5pms.Results.Files[r]]);
                    continue;
                }

                /* Set the API URL */
                let url = _5pms.Settings[_5pms.Settings.API].FileURL.replace('{{PROJECT}}', _5pms.Settings.Project) + encodeURIComponent(_5pms.Results.Files[r]) + '?ref=master';

                /* Get the data from the API */
                let result = await _5pms.Fetch(url);

                /* Transform the data */
                let content = atob(result.content);

                /* Split the content at the comment marks since we only want the comments */
                content = content.split('---');

                /* Break the comments at the any line breaks */
                content = content[1].split('\n');

                /* Remove the empty  parts of the array caused by the first and last /n */
                content.shift();
                content.pop();

                let data = {};

                /* Loop through the parts of the comment and break it into key value sets */
                content.forEach(key => {
                    /* Split the key from the value */
                    key = key.split(': ');

                    /* Split the tags into an array */
                    if (key[0] == 'tags') {
                        key[1] = key[1].substring(1, key[1].length-1);
                        key[1] = key[1].split(', ');
                    }

                    /* Add the data set back to the data object */
                    data[key[0]] = key[1];
                })

                /* Build the URL for the post */
                filename = _5pms.Results.Files[r].split(_5pms.Settings.Directory).pop();
                filename = filename.split('.').shift();
                /* filename = filename.join('.') */
                data.url = _5pms.Settings.Directory + filename;

                /* Set the content to the data */
                content = data;

                /* Add the data to the return set */
                results.push(content);

                /* Add result to the data for caching purposes */
                _5pms.Results.Data[_5pms.Results.Files[r]] = content;
            }

            /* Stop the loader */
            _5pms.Results._loader.Stop();
            
            return results;
        },

        /* Send a fetch request to the provided URL */
        Fetch: async (URL, Method = 'GET', Headers = []) => {
            /* Build headers */
            let headers = _5pms.Settings[_5pms.Settings.API].Headers;
            headers['PRIVATE-TOKEN'] = _5pms.Settings.Token;

            /* Make the fetch request */
            const response = await fetch(URL, {
                method: Method, // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                headers: headers,
                redirect: 'follow', // manual, *follow, error
                referrer: 'no-referrer', // no-referrer, *client
            });

            return await response.json();
        }
    }

    /* Initialize the FivePM object if it doesnt exist */
    if (typeof window.FivePM != "object") {
        window.FivePM = {};
    }

    /* Add the search module to FivePM */
    FivePM.Search = {
        Init: _5pms.Init,
        Query: _5pms.Query,
        Results: {
            Next: _5pms.Results.Next,
            Page: _5pms.Results.Page,
            Previous: _5pms.Results.Previous
        }
    };
})(window);