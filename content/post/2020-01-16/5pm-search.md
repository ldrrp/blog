---
title: The Road to Search (Part 3)
subtitle: So it's built.. 
excerpt: Setting up the 5pm search module to live and active use. - Part 3
image: https://5pm.dev/images/rts011620/search.jpg
tags: [Post, 5pm.dev, Search, Implementation]
date: 2020-01-16
author: Derrick Antaya
comments: false
---

# The Road to Search (Part 3)

Before I begin, if you havn't already take a look at how this idea started 
- [Idea](https://5pm.dev/post/2020-01-03/5pm-search/)
- [Build](https://5pm.dev/post/2020-01-15/5pm-search/)

## Recap

I needed a search solution for this blog that was both dynamic and ad free. My big idea was to build it out using the Git API to search directly through the repo, then build out the results page with some Javascript. This would be decently fast with a pretty good search request limit per user since the API is rate limited by IP.

Next I moved onto building the solution that came to be the 5pm Search module.

## Let's begin...

If I built this thing simple enough to implement this should be a rather short post.

### Step 1

You need to download the Javascript for the search module from the [repository](https://search.5pm.dev) and add it to your sites directory. I keep all my static files inside the ```/static/``` folder of my site, so the path to my search would be ```/js/search.js``` after its all compiled.

### Step 2

Once you have the file you need to include it somewhere on your page. I keep mine in the   ```<head></head> ``` sectin of my site, but a lot of people like to keep their scripts at the bottom for site speed reasons.

Once it is placed it should like somthing like this:

```html
<script src="https://5pm.dev/js/search.js"></script>
```

### Step 3

Now that you have it loaded up on your site its time to use it!

```js
FivePM.Search.Init({
    API: 'GitLab', // API to use
    Token: 'Personal Access Token Here', // Need to put your Personal Access Token you generated here
    Project: 'Project ID', // This is where you put your repository project ID
    Directory: '/post/', // The directory where your content lives in the repo
    Container: 'search-wrapper' // The ID of the dom element where you want your search to live
});
```

This little snippet will handle everything else for you as far as the search is concerned.

### Bonus

I hid my results in side a modal style window that is shown when a checkbox is checked. The label for the checkbox is a little search icon. Once you hit the search icon the checkbox gets checked and my search is displayed, javascript free.

Here is the HTML for somthing like this:

```html
<div class="menu-item search">
    <label id="search-open" for="search-toggle"><i class="far fa-search"></i></label>
    <input type="checkbox" id="search-toggle"/>
    <div id="search-container">
        <label id="search-close" for="search-toggle"><i class="fal fa-times"></i></label>
        <div id="search-wrapper"></div>
    </div>
</div>
```

*The ```<div id="search-wrapper"></div>``` is where the search is inserted by the 5pm Search Module.*

Here is the matching CSS

```css
#header #menu .menu-item.search #search-open {
    display: grid;
    justify-content: center;
    align-content: center;
    cursor: pointer;
}
#header #menu .search #search-toggle {
    display: none;
}
#header #menu .search #search-container {
    position: fixed;
    display: none;
    grid-template-columns: 1fr 90vw 1fr;
    grid-template-rows: 1fr 80vh 1fr;
    grid-template-areas:
        ". . toggle"
        ". search ."
        ". . .";
    width: 100vw;
    height: 100vh;
    top: 0;
    left: 0;
    background: rgba(0,0,0,0.5);
    cursor: default;
}
#header #menu .search #search-toggle:checked + #search-container {
    display: grid;
}
```

The CSS turns the ```#search-container``` to display grid when the checkbox is checked.

Thanks for reading and if you have any questions or comments use the links below to get intouch with me.

-- Derrick