---
title: The Road to Search (Part 1)
subtitle: Finding the right seach module for my blog.
excerpt: My path to finding the right search module for my blog and why I picked this one. - Part 1
image: https://5pm.dev/images/rts011620/search.jpg
tags: [Post, 5pm.dev, Search]
date: 2020-01-03
author: Derrick Antaya
comments: false
---

# The Road to Search

## Preface

So, I decided to start a blog. I found a name, I picked a design, I found the platform, I even started writing a couple posts. I was super excited because I was doing somthing new for me (for those who don't know me, I generally don't like to talk (I usually like to keep things short and to the point, some thing I am trying to work on starting this blog) so this is a big deal). A question popped into my head: *What am I going to use for searches?* 

I looked at the few options that Hugo has and they all seemed to generate a static index file used by JavaScript to build a search. My main issue with this is, what happens if (maybe even when) my blog gets rather large? It may be awhile but I feel like it would end up being clunky and slow.

Then I turned to Google site search, a decent option, they handle most of the load and they are quick, it doesn't even look half bad. BUT... there are ads. So much so that some times it seems like the half the first page of results are just sponsored content, and the only way to get rid of it is to pay Google to remove them. Seeing how I am not making any money on this site (yet) that would seem like a waste of money.

So, I was sitting at work one day, we had a helpdesk issue come in that our site was blowing up. A bot was hitting our search page and DDOSing the site. I thinking about why this was happeing and it popped into my head. I found an idea for a fully dynamic, ad free site search using the tools I already use for my site anyways. 

## Let's begin...

To start things off, my site (this blog) is built using a custom Hugo theme, hosted on GitLab Pages in a public repository and built with the GitLab CI upon commit to the master branch.

I started looking at the GitLab API to see if I could find anything usefull here, and I struck gold. *Spoiler: If you havn't already guessed my idea was to use the Git API to build and run my search engine.*

The concept is pretty simple and in practice turned out to not be so complicatd either, more on that in the next post.

I sat down and started writing, a few days later the [5pm Search Module](https://search.5pm.dev) was ready for testing.

The basics of how it works are pretty simple.

- Add some JavaScript to the page
- Initialize the script passing in some variables needed to operate
- Boom! - an ad free custom search that works with your Hugo repository

Now it may be a little more complicated than that. There is some setup work you need to do on your blog to add it in, and on your content pages for it build the results properly which I'll cover in part 2.

Some things to note about the [5pm Search Module](https://search.5pm.dev):
- It is still in early version
- The code base is pretty messy
- It currently only supports the GitLab API (full Git support coming soon)
- It may not be the most performant system out there, but it is pretty quick and I am working on making it better and faster
- It is open source so feel free to use it on your site and even help improve it
- It is still missing some basic features which are in the works

Continue to part 2 [here](https://5pm.dev/post/2020-01-15/5pm-search/).

Thanks for reading and if you have any questions or comments use the links below to get intouch with me.

-- Derrick