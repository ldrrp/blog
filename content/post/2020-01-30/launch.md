---
title: Launch.. Fail.. Repeat..
subtitle: Launching of a flawed update.
excerpt: The story of a code release gone wrong, and what we learned from it.
image: https://5pm.dev/images/bnr013020/balloon.jpg
tags: [Post, 5pm.dev, BNTech, Go, React, Dev Ops, Memory, Late Night]
date: 2020-01-30
author: Derrick Antaya
comments: false
---

# Launch.. Fail.. Repeat..

### The Start

The team had been working on a new subscribe platform for [The Buffalo News](https://buffalonews.com) for the last several months. 
The main goal of the project was to streamline the subscription process and make it easier for users to sign up. The front end of the 
project is written using React.js with the API being added to our Golang base. We have been building 
and testing on repeat almost the whole time. Stake holders would get their hands on it and request more changes or sometimes even undoing 
requests they made previously. This is unfortunate but it's the way things roll in our industry right?

The project was already delayed several times because changes were introduced or misunderstanding of external APIs caused issues within the code base.
We had been pushing hard to get this project wrapped up because it had gone on and on. We put in late nights and extra hours in the final stretch.
We got most of the bugs worked out and were ready to go.

So we started setting things up to get ready for a smooth launch.

- All of our settings in the database updated to the new structure
- Settings on the servers were updated to the new structure
- Build files were pulled to the servers

All we needed to do is push the built front end and restart our API service on each of the servers.

### The Launch

Everything had been tested to the best of the teams ability at the time, and even introduced to external teams for testing. Everything was setup 
on the servers and ready to go ( *or so we thought* ). 

We set the go live time for 9:30pm to avoid the main rush of traffic to the site and try to mitigate any loss of data that may happen if somthing goes wrong.

In theory all we would need to do is restart the rename some folders and restart our services.

So we renamed our folders and tried to restart our services...

### The Fail

... and the service didn't come back up. Into panic mode I went. 

- What happened?
- Why were the servers staying down? We already tested this.

Pulled up the logs for the servers. I had forgotten to copy the new settings from our shared drive. Easy fix. I pulled the new files, started the service. 
It came up, then went down. Now I was getting an error saying the ecoding on our json files was wrong. I quickly inspected them and they seemed right, they passed a linter. 

I vaugly remember seeing this error in the past, shit, it was a Byte order mark (BOM) encoding issue in the utf8 json files. This was caused by an encoding setting when
I saved the updated files on my machine, before sending them to the shared server. We quickly rand a script accross all our settings files to remove the BOM.

```bash
sed -i '1s/^\xEF\xBB\xBF//' settings.json
```

Removes the BOM from the file and saves over the original.

Pulled the updated files down to the server and tried to start the services again. They started this time. For a few minutes..

New error this time, and error I have never seen before my entire time working with Go.

```bash
fatal error: newproc: function arguments too large for new goroutine
```

So this time, after a little research we found out this fatal is thrown when you a very large object being passed arround in the system. We were effectivly running
out of memory on the stack.

At this point it was almost midnight and we decided to rollback so we could assess the cause of this error in the morning. Rolling back was easy since we made sure to 
make copies of everything before making any changes.

### The Repeat

The following morning, I was able to track down the cause of the fatal error and it pointed back to 2 things

1. Another mistake I made was not minifying the settings files for the system, so it was trying to read in settings files that were over 100kb in size.
2. A few places inside the code we were passing the whole settings arround into various functions

Both fixes were pretty simple after I had some sleep. 

First I minified the files cutting their size down to about 40kb. 
Second I adjusted the functions in the API to use pointers to the settings so we were not duplicating the settings stored in 
memory ( *it turns out it was about 4 functions* ). The 4 functions I had to change meant that the settings would be read 1 time and not copied 
at all for that request. I guess 4 copies of settings per request makes a big difference on an API thats running about 10 requests a second in during our slow time.
( *We added load testing to our testing suite now* )

After the changes and some quick load testing I felt we were ready to try again. We got the team together at the same time as the night before. 
Made sure we didn't replicate the same dumb mistakes I made the first time.

9:30pm rolled arround and we made the changes, restarted the services, and.. they stayed up this time. That right away was an instant win. We started testing all the new
feauteres to make sure they were working properly. Most of them were, we had some issues we were able to sort out quickly enough to leave it up. 

After 2 longs nights and what we considered a successful launch ( *although it was terrible* ) we went to bed arround 12am.

## The End

The next morning the first thing I did was check my email because I was expecting that somthing had gone wrong while I was out. We had 1 single helpdesk ticket.

We had done it, finally. After 9 months of hard work, missing almost all of our deadlines ( *sometimes our fault, sometimes not* ) it was finally done. 

Feel free to check it out [BNTech Subscribe](https://subscribe.buffalonews.com).

## Lessons

After a gruling process I deffinately learned some hard but valuable lessons.

1. Never rush things or wait till the last minute even if you think they are easy and wont take that long.   
   If I didn't rush the updating our settings files I probably would have caught the BOM error 
2. Don't forget testing  
   There are always things we think about while developing but I'm sure I'm no the only one that puts stress testing towards the back of the list. I learned the hardway
 that stress testing is important. Had we load tested the system, probably even half of the actual load the of the API we would have caught the memory error. This is made 
 even more difficult by the fact that our team is not very big, so sparing the resources to think about testing like this can be difficult.

Thanks for reading and if you have any questions or comments use the links below to get intouch with me.

– Derrick