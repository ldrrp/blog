---
title: The Road to Search (Part 2)
subtitle: I had the idea.. now what?
excerpt: Building the solution that became search.5pm.dev. - Part 2
image: https://5pm.dev/images/rts011620/search.jpg
tags: [Post, 5pm.dev, Search, Solution]
date: 2020-01-15
author: Derrick Antaya
comments: false
---

# The Road to Search (Part 2)

Before I begin, if you havn't already take a look at how this idea started [here](https://5pm.dev/post/2020-01-03/5pm-search/).

## Recap

I needed a search solution for this blog that was both dynamic and ad free. My big idea was to build it out using the Git API to search directly through the repo, then build out the results page with some Javascript. This would be decently fast with a pretty good search request limit per user since the API is rate limited by IP.

## Let's begin...

Once I had the idea I started doing some the research into the Gitlab API to make sure what I wanted was possible. I started reading their their docs, which are pretty well done. You can find them [here](https://docs.gitlab.com/ee/api/). The main focus for me was on their [Search](https://docs.gitlab.com/ee/api/search.html) and [File](https://docs.gitlab.com/ee/api/repository_files.html) APIs.

Once I did the main research I found out that the things I needed to make this happen were:
- The project ID for the repo that I was trying to search through
- A personal access token that would allow me access to the API for the repo.

## The project ID

Getting the project ID is easy. It is listed right under the repository title on the main page of the repo.

![alt text](https://5pm.dev/images/rts011620/projectid.png "Gitlab Project ID")

Done! Well that was a little easy, let's try somthing a bit more challenging.

## The access token

Getting an access token is not very difficult either, if you know where to look for it. 
After logging into [Gitlab.com](https://gitlab.com) you just head over to your User Settings page under your icon on the right of the header. Once on the page you will see a menu link to the page for Access Tokens. To make this work properly it seems like you need an access token that has API level access. Now that you just read that, you can just go to the [Personal Access Tokens](https://gitlab.com/profile/personal_access_tokens) page.

***Big draw back here -*** *once you create an access token with API access and displaying it on the front end of a website with Javascript you are essentially giving people access to your main Gitlab account with full access to all your repos.*

With that out of the way, here is how I went about resolving this issue. I created a new Gitlab account using a special email I created. Then I added that account to the repo as a member with guest access. I have my repo settings so that any merges are restricted to code owners (me) so this new account can't make any catastrophic changes.

Since my repo is public that should be enough. Everyone is allowed to view my repo and even request changes.

*before doing this I would reccomend you go over your repo security settings and access roles as well as make sure your new account is secure.*

*I also recomend changing out your access token every so often just to be safe.*

## Next..

Now I have the 2 pieces of information I needed to begin, my project ID and an account with API level access token so I can use the API. let's get to the fun part.

## Builing seearch.

Search is currently built with plain Javascript, with plans to turn it into a TypeScript project at some point ( when I have a bit more time ). It was also built a bit lazily since I was just trying to get it working first.

Search is built as a single object that takes some settings when it is run. The settings define things like which API to use and what your package ID and user token is. 

The settings look a little like this:

```js
{
    API: 'GitLab', // Defines the API
    Token: 'WgqSAzyASX5pxRnxQG2s', // User Access Token
    Project: '13925063', // Git repo project ID
    Directory: '/post/', // The directory in the repo that is defined as a post
    Container: 'search-wrapper' // The id of the container we want to sink the search into
}
```

*Notes:*
- *Only supported API is Gitlab right now, plans for Github in the future*
- *Directory is used so that files outside of the main content are exluded from the results*

The basic implementation handles all the work for you, but there are exposed functions so you can built your own custom experiance if you would like to.

It should be pretty straight forward to follow along, its only about 650 lines including comments, whitespace and the dom.

You can check it out [here](https://search.5pm.blog). Feel free to help out if you see any issues or things that could be done better, just fork it and make some pull requests.

I'm going to write up a small post about the exact implementation in part 3 so stay tuned: Check it out [here](https://5pm.dev/post/2020-01-16/5pm-search/).

Thanks for reading and if you have any questions or comments use the links below to get intouch with me.

-- Derrick