---
title: Welcome to 5pm.dev
subtitle: A little about myself
excerpt: Welcome to 5pm.dev, let me tell you a bit about myself.
image: https://5pm.dev/images/derrick.jpg
tags: [Welcome, Post, Introduction, 5pm.dev]
date: 2020-01-01
author: Derrick Antaya
comments: false
---

# Welcome to 5pm.dev

Hello, my name is Derrick. I'd like to tell you a little about myself. 

I currently work at [The Buffalo News](https://buffalonews.com) as the development manager were I oversee our development team and help with the digital product roadmap. I was the pioneer developer on our Paywall system which has since grown into a full analytics system that is in use on multiple news sites accross the country right now (more on that later).

I also have a side business tinroof LLC. Here is were I tackle any of my side projects which will be covered here in the future. *Spoiler alert this blog is one of them.*

**So why am I here?**
- I thought I'd try my hand at blogging and see if I can get into it.

**Where did the name come from?**
- I was looking for somthing relativly short that fit the topics I plan to cover. I found a nice name and started creating the site. *More posts will come and will be tailored based on the building and setup of the site*

In the meantime the basic layout of the site is:

- Built using Hugo
- Created a custom theme
- Setup ci with gitlab ci 
- Hosted with gitlab pages

I started looking at search options and the main one that seemed to work was google site search, but it came with ads that can't be removed unless I paid. I didn't see the point of paying for search on a blog that I most likly won't be making money on. So I decided to write my own, more on that in a future post. 

I have a few more things I'd like to try out here so I hope you stay tuned.

Comments will be coming soon, I hope. In the meantime you can send any emails to dantaya@tinroof.co I look forward getting better at this as time goes on.

Thanks for reading,

-- Derrick