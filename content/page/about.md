---
title: About me
subtitle: 
comments: false
---

# About Me

<div id="about-me">
    <img src="/images/derrick.jpg" align="left" />

Hello, my name is Derrick Antaya, I am the founder of Tin Roof llc. and the current development manager at The Buffalo News, where I started the development of our custom built paywall and analytics system in 2015 (which is now BNTech). My passion for development and data drive what I do every day and is the reason I love my job. 

Outside of work I like to spend my time with my family outside and working on my house (it's been a long project).

</div>

# Resume

## Experience

<div class="single-tab">

### Tin Roof llc.

**CEO**  
*Aug 2017 - Present*
> My personal company that was formed to keep track of any of my projects or side jobs.

### The Buffalo News

**Software Development Manager**  
*Aug 2019 - Present*
> Manage a team of 7 developers accross The Buffalo News and BNTech teams to architect and design solutions and mentor the team.

**Software Engineer**  
*Dec 2015 – Aug 2019*
> Work on buffalonews.com building features for our custom wordpress installation, using PHP and JS.

### BN Tech

**Lead Software Engineer**  
*Apr 2018 - Present*
> Architect and build out a paywall and analytics system using GO and JS. It has since grown into multiple parts including a single sign on, subscribtion platform and is run as a SaaS product that handles over 500,000,000 requests a month.

### Computer SOS Inc.

**Backend Web Developer**  
*May 2014 - Dec 2015*
> Worked on custom CMS and Sports league managment software in PHP.

</div>

## Skills

<div class="single-tab">

GO - Javascript - HTML - CSS - Elasticsearch - Kafka - PHP - MYSQL - API - Rest - Big Data - Analytics

</div>

## Projects

<div class="single-tab">
<div class="project" id="five-pm-dev">

### 5pm.{dev}
> A blog where I can come to unwind from a long day at work.

</div>

<div class="project" id="aerate">

### Aerate
> Rainwater collection and utilization system for houses.
> The project started as a way to get around having sulfer water come from my well. I designed a system that will collect rain water from my roof and store it in a tank in my basement. Using sensors the system will detect if there is enough water in the rain tank and use that to fill my hot water tank and supply the cold water. In the event there is not enough rain water it will open a valve to my well water.
> The system will be built using water flow sensors, valves and pressure sensors with a raspberry pi, and completly coded in GO.

</div>
</div>

## Education

<div class="single-tab">

### University at Buffalo

BS. Computer Science
2011-2014

</div>

## Get in touch

